.. _opnfv-installation:

.. This work is licensed under a Creative Commons Attribution 4.0 International License.
.. SPDX-License-Identifier: CC-BY-4.0
.. (c) Anuket CCC, AT&T, and other contributors

============
Installation
============

Abstract
========

This is the installation document for the Anuket Kali release, please use this document for further reference. 

Installation Procedure
======================

Each of the test Frameworks and other projects will have separate installations procedures: see the individual project documentation.

- :ref:`Airship Installation Guide <airship-installation>`
- :ref:`Barometer Installation Guide <barometer-installation>`
- :ref:`CIRV Installation Guide <cirv-installation>`
- :ref:`Kuberef Installation Guide <kuberef-installation>`
- :ref:`ViNePERF installation Guide <vineperf-installation>`

Note: links in this list need updating

Anuket Test Frameworks
=====================

If you have elected to install the Anuket platform using the deployment toolchain provided by Anuket,
your system will have been validated once the installation is completed.
The basic deployment validation only addresses a small part of capabilities in
the platform and you may want to execute more exhaustive tests. Some investigation will be required to
select the right test suites to run on your platform.

Many of the Anuket test projects provide user-guide documentation and installation instructions in :ref:`this document <testing-userguide>`
